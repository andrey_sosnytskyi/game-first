<?php
defined( 'ABSPATH' ) || exit;
global $mysqli;
if (isset($_GET['id'])) {
    $user = $mysqli->query('SELECT * FROM users WHERE id=' . $_GET['id']);
    ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 card">
                <div class="card-header bg-primary text-white">
                    User view
                </div>
                <?php
                $user_info = $user->fetch_object();
                ?>
                <div class="card-body">
                    <div class="form-group">
                        <label>ID</label>
                        <input type="text" class="form-control" disabled="disabled" value="<?= $user_info->id; ?>">
                    </div>
                    <div class="form-group">
                        <label>Login</label>
                        <input type="text" class="form-control" disabled="disabled" value="<?php echo $user_info->username ?: "Not filled"; ?>"/>
                    </div>
                    <div class="form-group">
                        <label>First Name</label>
                        <input type="text" class="form-control" disabled="disabled" value="<?php echo $user_info->first_name ?: "Not filled"; ?>"/>
                    </div>
                    <div class="form-group">
                        <label>Last Name</label>
                        <input type="text" class="form-control" disabled="disabled" value="<?php echo $user_info->last_name ?: "Not filled"; ?>"/>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" class="form-control" disabled="disabled" value="<?php echo $user_info->email ?: "Not filled"; ?>"/>
                    </div>
                    <div class="form-group">
                        <label>Scores</label>
                        <input type="text" class="form-control" disabled="disabled" value="<?php echo $user_info->scores ?: "Not filled"; ?>"/>
                        </div>
                    <div class="form-group">
                        <label>Phone</label>
                        <input type="text" class="form-control" disabled="disabled" value="<?php echo $user_info->phone ?: "Not filled"; ?>"/>
                    </div>
                    <div class="form-group">
                        <label>Status</label>
                        <input type="text" class="form-control" disabled="disabled" value="<?= $user_info->status; ?>"/>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <?php
}
