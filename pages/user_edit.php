<?php
defined( 'ABSPATH' ) || exit;
global $mysqli;
if (isset($_GET['id'])) {

    if(isset($_POST['username'])){
        $user_id = $_GET['id'];
        $username = $_POST['username'];
        $password = $_POST['password'];
        $password_hash = md5($password);
        $first_name = $_POST['first_name'];
        $last_name = $_POST['last_name'];
        $email = $_POST['email'];
        $phone = $_POST['phone'];
        $scores = $_POST['scores'];
        $status = $_POST['status'];
        /*
         *
         *
         *   UPDATE table_name
        SET column1 = value1, column2 = value2, ...
        WHERE condition;
         *
         * */
        if(!empty($password)) {
            $upd = $mysqli->query("
        UPDATE 
            users
        SET 
        username = '" . $username . "',
        password = '" . $password_hash . "',
        first_name = '" . $first_name . "',
        last_name = '" . $last_name . "',
        email = '" . $email . "',
        phone = '" . $phone . "',
        scores = '" . $scores . "',
        status = '" . $status . "'
        WHERE
            id =" . $user_id);
        }else{
            $upd = $mysqli->query("
        UPDATE users
        SET 
        username = '" . $username . "',
        first_name = '" . $first_name . "',
        last_name = '" . $last_name . "',
        email = '" . $email . "',
        phone = '" . $phone . "',
        scores = '" . $scores . "',
        status = '" . $status . "'
        WHERE
            id =" . $user_id);
        }
        echo '<div class="alert alert-success alert-dismissible fade show" role="alert">
  <strong>Success!</strong> Data saved!
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>';
    }


    $user = $mysqli->query('SELECT * FROM users WHERE id=' . $_GET['id']);

    ?>

    <div class="container-fluid">
        <div class="row">
            <div class="col-12 card">
                <div class="card-header bg-primary text-white">
                    User view
                </div>
                <?php
                $user_info = $user->fetch_object();
                ?>
                <div class="card-body">
                    <form method="post">
                        <div class="form-group">
                            <label>ID</label>
                            <input type="text" class="form-control" disabled="disabled" value="<?= $user_info->id; ?>">
                        </div>
                        <div class="form-group">
                            <label>Login</label>
                            <input type="text" name="username" class="form-control"  value="<?php echo $user_info->username; ?>"/>
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="text" name="password" class="form-control"  value=""/>
                            <small>Leave empty if you don`t want to change password</small>
                        </div>
                        <div class="form-group">
                            <label>First Name</label>
                            <input type="text" name="first_name" class="form-control"  value="<?php echo $user_info->first_name; ?>"/>
                        </div>
                        <div class="form-group">
                            <label>Last Name</label>
                            <input type="text" name="last_name" class="form-control"  value="<?php echo $user_info->last_name; ?>"/>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" name="email" placeholder="Enter email..." class="form-control"  value="<?php echo $user_info->email; ?>"/>
                        </div>
                        <div class="form-group">
                            <label>Scores</label>
                            <input type="text" name="scores" placeholder="Enter scores..." class="form-control"  value="<?php echo $user_info->scores; ?>"/>
                            </div>
                        <div class="form-group">
                            <label>Phone</label>
                            <input type="text" name="phone" placeholder="Enter phone..." class="form-control"  value="<?php echo $user_info->phone; ?>"/>
                        </div>
                        <div class="form-group">
                            <label>Status</label>
                            <select name="status" class="form-control">
                                <option value="admin" <?php if($user_info->status == "admin") { echo  'selected="selected"'; } ?> >Administrator</option>
                                <option value="user" <?php if($user_info->status == "user") { echo  'selected="selected"'; } ?> >User</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success btn-block">Save Changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <?php
}