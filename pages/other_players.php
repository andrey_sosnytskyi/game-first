<?php

global $mysqli;
require_once '../system/classes/User.php';
require_once '../system/configuration.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <title>Other players</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header bg-success">
                    <span class="text-white">Other players</span>
                </div>
                <div class="card-body">
                    <table class="table table-striped">
                        <thead class="thead-dark">
                        <tr>
                            <th>ID</th>
                            <!--<th>Login</th> -->
                            <th>Full Name</th>
                            <th>Scores</th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $all_players = $mysqli->query("SELECT * FROM users ORDER BY id ASC ");

                        while ($user = $all_players->fetch_object()) {
                            ?>
                            <tr>
                                <td><?= $user->id; ?></td>
                                <td><?= $user->first_name . "&nbsp;" . $user->last_name; ?></td>
                                <td><?php echo $user->scores ?: "Not filled"; ?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <a class="btn btn-primary" href="http://game.local/index.php?page=main" style="margin-top: 20px;margin-right: 70%;margin-left: 1%">Main page</a>
</div>

</body>
</html>
