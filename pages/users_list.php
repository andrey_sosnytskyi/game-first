<?php
defined( 'ABSPATH' ) || exit;
global $mysqli;

if(isset($_GET['action']) && $_GET['action'] == "delete"){
    $mysqli->query("DELETE FROM users WHERE id='".$_GET['id']."'") or die($mysqli->error);
    echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
  <strong>Success!</strong> User with ID = '.$_GET['id'].' have been removed!
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>';
}

?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header bg-success">
                    <span class="text-white">Users</span>
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Login</th>
                            <th>Full Name</th>
                            <th>E-mail</th>
                            <th>Scores</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $all_users = $mysqli->query("SELECT * FROM users ORDER BY id ASC ");

                      while ($user = $all_users->fetch_object()) {
                            ?>
                            <tr>
                                <td><?= $user->id; ?></td>
                                <td><?= $user->username; ?></td>
                                <td><?= $user->first_name . "&nbsp;" . $user->last_name; ?></td>
                                <td><?php echo $user->email ?: "Not filled"; ?></td>
                                <td><?php echo $user->scores ?: "Not filled"; ?></td>
                                <td>
                                    <a href="/?page=user_view&id=<?= $user->id; ?>" class="btn btn-warning">View</a>
                                    <a href="/?page=user_edit&id=<?= $user->id; ?>" class="btn btn-primary">Edit</a>
                                    <a href="/?page=users_list&action=delete&id=<?= $user->id; ?>"
                                       class="btn btn-danger">Delete</a>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

