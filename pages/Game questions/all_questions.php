<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en,ru" xmlns="http://www.w3.org/1999/html">
<head>
    <title>
    </title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
     <link rel="stylesheet" href="/pages/Styles/all_questions.css">

</head>
<body>
<div style="background: url('/resources/quiz question 1.jpg') no-repeat;width: 100%;height:100vh;background-size: cover;">

<div class="question-number">
    <h2>Question
        <span id="number-of-question"></span>
        from
        <span id="number-of-all-questions"></span>
    </h2>
</div>

<h2 class="h2" align="center"><span>A</span><span>n</span><span>s</span><span>w</span><span>e</span><span>r</span><span>:</span></h2>
<!--<div class="timer-container progress"></div> -->
    <progress  id="timeBar" class="progress-bar" value="100" max="100" style="width: 80%; margin-left: 10%" ></progress>
  <!-- <div id="timeBar" class="progress-bar" style="width: 0%;" ></div> -->


<div class="container bg-info col-xs-10 col-md-8 col-sm-10 col-lg-8 col-xl-8">
    <p id="question" class="text-center"></p>
    <div class="question-image">

    </div>
<!--
    <div class="col-12">
        <img id="logo1" class="img-thumbnail"  src="/resources/toyota_logo1.jpg" alt="Photo1">
        <span id="paragraph">And</span>
        <img id="logo2" class=" img-thumbnail"  src="/resources/toyota_logo2.jpg" alt="Photo2">
    </div> -->
<!--
<div class="options">
    <div data-id="0" class="option option1"></div>
    <div data-id="1" class="option option2"></div>
    <div data-id="2" class="option option3"></div>
    <div data-id="3" class="option option4"></div>
</div> -->
<!-- class = "invisible"-->

    <div class="card" id="card">
        <div class="card-body ">

                <div class="form-check">
                    <label>
                        <button data-id ="0" class="btn  btn-secondary  input1 quizBtn " type="button"   name="button1"  ></button>
                    </label>
                </div>
                <div class="form-check">
                    <label>
                    <button data-id ="1" class="btn  btn-secondary input2 quizBtn " type="button"   name="button1"></button>
                    </label>
                </div>

                <div class="form-check">
                    <label>
                    <button data-id ="2" class="btn  btn-secondary input3 quizBtn" type="button"  name="button1" ></button>
                    </label>
                </div>
                <div class="form-check">
                    <label>
                    <button data-id ="3"  class="btn  btn-secondary input4 quizBtn" type="button"  name="button1"></button>
                    </label>
                    </div>
            </div>
    </div>
    <hr>
    <div id ="answers-tracker">
</div>
   <script>
        $(document).ready(function () {

            $('.quizBtn').click(function () {
                $('button[name="button1"]').prop('disabled', true);
            });
            $('#btn-next').click(function (){
                $('button[name = "button1"]').prop('disabled',false);
            });
        });

    </script>
    <div class="button">
        <button id="btn-next">Next</button>
    </div>
</div>

       <div class =" modal fade show quiz-over-modal invisible " style="display: block;" tabindex="-1" role="dialog" aria-hidden="true"  >
            <div class="modal-dialog content" role="document">
                <div class="modal-content">
                    <div class="modal-header">

                        <p>Вы ответили правильно на
                            <span id="correct-answer"></span> вопросов из
                            <span id ="number-of-all-questions-2"></span>
                        </p>

                    </div>

                    <div class="modal-body">
                        <h3 align="center" id ="alert"></h3>
                        <p id="success"></p>
                        <p id ="percent">Ваш прогресс составляет:
                            <span id="percentage"></span>%
                        </p>

                        <p>Осталось попыток:
                            <span id="attempts"></span>
                        </p>
                        <a href="/pages/other_players.php" style="margin-left: 85%;font-size: 1.2em;">Others</a>
                    <!--
                     <form id="form" method="post">

                        <label id ="modal_label"> Введите свое имя,чтобы сохранить прогресс

                        <input class="form-check col-md-3" name="name"  id="user_name" type="text" value=""/>

                        </label>
                        <button type="submit" class="btn btn-info" id="scoreBtn" >Save Progress</button>

                    </form> -->
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-warning"  id="btn-try-again" >Try again</button>
                        <button class="btn btn-primary saveProgress" onclick="getdetails()">Save progress </button>
                        <button class="btn btn-success" ><a href="http://game.local/index.php?page=main" style="color: white">Main page</a></button>
                    </div>
                </div>
            </div>
       </div>

    <!-- Modal -->
   <!-- <div class="modal fade show quiz-over-modal invisible" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div> -->

       <script src="/JS/main.js"></script>
    <script src="/JS/async%20functions.js"></script>

    <script>
      function getdetails() {
          var correctAnswers = $('#correct-answer').val();
          $.ajax({
              type:"POST",
              url: "/pages/php_listener/async functions.php",
              data: {
                  action: "update_user_score",
                  uid: "<?=$_SESSION['uid']; ?>",
                  results: score
              }
          });
      }
    </script>

    <script>
       let header = document.getElementById('alert');
        let saveProgress = document.querySelector('.saveProgress');
        saveProgress.addEventListener('click' ,function () {
            header.innerHTML = '<div class="alert alert-success alert-dismissible fade show" role="alert"> <strong>Success!</strong> Data saved! <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button> </div>';
        });
    </script>
<?php
require_once 'pages/other_players.php';
?>
</body>
</html>

