<!DOCTYPE html>
<html lang="en">
<head>
    <title>
    </title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="/JS/seperate%20questions/EventListener_question2.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src=" https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="/pages/Styles/question2.css">
</head>
<body style="background: url('/resources/quiz question 2.jpg') no-repeat;width: 100%;height:100vh;background-size: cover;">
<div class="question-number">
    <h2>Question
        <span id="number-of-question"></span>
        from
        <span id="number-of-all-questions"></span>
    </h2>
</div>

<h2 class="h2" align="center"><span>A</span><span>n</span><span>s</span><span>w</span><span>e</span><span>r</span><span>:</span></h2>
<div class="container bg-success col-xs-10 col-md-8 col-sm-10 col-lg-8 col-xl-8">

    <p class="text-center"> Which sign you need to put between numbers 4 and 5 so that the result
        is the number more than four but less than five ? </p>

    <div class="card" id="card">
        <div class="card-body">

<label>
    Please,put your answer here:
</label>
            <form action="/pages/php%20listener/async%20functions.php" method="post">
            <div class="form-inline">
    <input class="form-control  col-xs-10 col-md-8 col-sm-10 col-lg-8 col-xl-8 input" type="text" name="input_field" value=""/>
                <button class="btn btn-info check">Check</button>
            </div>
            </form>
        </div>
    </div>
    <script>
        $(document).ready(function () {


            $('.input').click(function () {
                setTimeout(function () {

                    $('input[name="input_field"]').prop('disabled', true);
                }, 7000);
            });
                $('input[name="input_field"]').on('change', function () {
                    $('.btn-primary').removeAttr('hidden');
                });
            });

    </script>
    <a href="/pages/Game%20questions/question3.php" class="btn btn-primary" hidden>next question</a>

</body>
</html>

