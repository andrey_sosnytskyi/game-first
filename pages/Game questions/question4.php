<!DOCTYPE html>
<html lang="en">
<head>
    <title>
    </title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="/JS/seperate%20questions/EventListener_question4.js"></script>
    <script src="/JS/main.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src=" https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="/pages/Styles/question4.css">
</head>
<body style="background: url('/resources/quiz question 4.jpg') no-repeat;width: 100%;height:100vh;background-size: cover;">

<div class="question-number">
    <h2>Question
        <span id="number-of-question"></span>
        from
        <span id="number-of-all-questions"></span>
    </h2>
</div>

<h2 class="h2" align="center"><span>A</span><span>n</span><span>s</span><span>w</span><span>e</span><span>r</span><span>:</span></h2>
<div class="container bg-secondary col-xs-10 col-md-8 col-sm-10 col-lg-8 col-xl-8">

    <p class="text-center"> What is the most spoken language in the world ? <span>&#129300;</span> </p>

    <div class="card" id="card">
        <div class="card-body">
            <div class="form-check">
                <label id="label" class="text-danger">
                    <input class="form-check-input" type="radio" id="input1" name="radio1" value="wrong!"> English
                </label>
            </div>
            <div class="form-check">
                <label id="label" class="text-danger">
                    <input class="form-check-input " type="radio" id="input2" name="radio1" value="right! &nbsp; perfect!"> Chinese
                </label>
            </div>
            <div class="form-check">
                <label id="label" class="text-danger">
                    <input class="form-check-input " type="radio" id="input3" name="radio1" value="wrong!"> Spanish
                </label>
            </div>
            <div class="form-check ">
                <label id="label" class="text-danger">
                    <input class="form-check-input " type="radio" id="input4" name="radio1" value="wrong!"> Arabic
                </label>
            </div>
        </div>

    </div>
    <script>
        $(document).ready(function () {


            $('.form-check-input').click(function () {
                $('input[name="radio1"]').prop('disabled', true);
            });

            $('input[name="radio1"]').on('change',function () {
                $('.btn-primary').removeAttr('hidden');
            });
        });
    </script>
    <a href="/pages/Game%20questions/question5.php" class ="btn btn-primary" hidden> next question</a>

</body>
</html>


