<!DOCTYPE html>
<html lang="en">
<head>
    <title>
    </title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="/JS/seperate%20questions/EventListener_question3.js"></script>
    <script src="/JS/main.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src=" https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="/pages/Styles/question3.css">
</head>
<body style="background: url('/resources/quiz question 3.jpg') no-repeat;width: 100%;height:100vh;background-size: cover;">

<div class="question-number">
    <h2>Question
        <span id="number-of-question"></span>
        from
        <span id="number-of-all-questions"></span>
    </h2>
</div>

<h2 class="h2" align="center"><span>A</span><span>n</span><span>s</span><span>w</span><span>e</span><span>r</span><span>:</span></h2>
<div class="container bg-secondary  col-xs-10 col-md-8 col-sm-10 col-lg-8 col-xl-8">

    <p class="text-center"> Who is the Direct Manager ? <span>&#129311;</span></p>

    <div class="card" id="card">
        <div class="card-body">
            <div class="form-check">
                <label id="label" class="text-primary ">
                    <input class="form-check-input" type="radio" id="input1" name="radio1" value="wrong!"> This is a boss involved in managing process and personnel
                    in a certain area of an enterprize,organization.May be the owner,but is often an employee.
                </label>
            </div>
            <div class="form-check">
                <label id="label" class="text-primary ">
                    <input class="form-check-input " type="radio" id="input2" name="radio1" value="wrong!"> This is a person who creates invoices, bills, invoices, cash receipts, payment orders for the bank, and others and calculates taxes.
                    He makes payments through the bank in favor of other firms with which this company cooperates
                </label>
            </div>
            <div class="form-check">
                <label id="label" class="text-primary ">
                    <input class="form-check-input " type="radio" id="input3" name="radio1" value="wrong!"> This is a person who makes commercial and management decisions for the effective operation of an online store. At the same time, the main guideline is the satisfaction of the requests and needs of customers.
                    It also ensures the continuous operation of the online store, without interruptions and
                    draws up an online store activity plan, identifies measures that need to be taken to improve work efficiency.
                </label>
            </div>
            <div class="form-check ">
                <label id="label" class="text-primary ">
                    <input class="form-check-input " type="radio" id="input4" name="radio1" value="right!"> This is a person who professionally communicates with potential customers in personal messages on social networks, represents
                    the interests of the Instagram store and provides all the data about the product that the buyer is interested in
                </label>
            </div>
        </div>

    </div>
    <script>
        $(document).ready(function () {


            $('.form-check-input').click(function () {
                $('input[name="radio1"]').prop('disabled', true);
            });

            $('input[name="radio1"]').on('change',function () {
                $('.btn-primary').removeAttr('hidden');
            });
        });
    </script>
    <a href="/pages/Game%20questions/question4.php" class="btn btn-primary" hidden>next question</a>
</body>
</html>

