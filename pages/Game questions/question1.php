<!DOCTYPE html>
<html lang="en">
<head>
    <title>
    </title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="/JS/seperate%20questions/EventListener_question1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="/pages/Styles/question1.css">
</head>
<body style="background: url('/resources/quiz question 1.jpg') no-repeat;width: 100%;height:100vh;background-size: cover;">

<div class="question-number">
    <h2>Question
        <span id="number-of-question"></span>
        from
        <span id="number-of-all-questions"></span>
    </h2>
</div>

<h2 class="h2" align="center"><span>A</span><span>n</span><span>s</span><span>w</span><span>e</span><span>r</span><span>:</span></h2>
<div class="container bg-warning col-xs-10 col-md-8 col-sm-10 col-lg-8 col-xl-8">

    <p id="question" class="text-center">What the difference between these pictures ?</p>

    <div class="col-12">
    <img id="logo1" class="img-thumbnail"  src="/resources/toyota_logo1.jpg" alt="Photo1">
    <span id="paragraph">And</span>
    <img id="logo2" class=" img-thumbnail"  src="/resources/toyota_logo2.jpg" alt="Photo2">
    </div>



    <div class="card" id="card">
        <div class="card-body">
            <div class="form-check">
                <label id="label" class="text-success ">
                    <input class="form-check-input" type="radio" id="input1" name="radio1" value="wrong!">There is no difference,only the first logo an older sample
                </label>
            </div>
            <div class="form-check">
                <label id="label" class="text-success ">
                    <input class="form-check-input " type="radio" id="input2" name="radio1" value="wrong!">The first logo is used for a lower car class compare to the second one
                </label>
            </div>
            <div class="form-check">
                <label id="label" class="text-success ">
                    <input class="form-check-input " type="radio" id="input3" name="radio1" value="right!">The second logo is used to identify electric vechiles of this brand
                </label>
            </div>
            <div class="form-check ">
                <label id="label" class="text-success ">
                    <input class="form-check-input " type="radio" id="input4" name="radio1" value="wrong!">The second logo is used to identify premium-class cars of this brand
                </label>
            </div>
        </div>

</div>

<script>
    $(document).ready(function () {

        $('.form-check-input').click(function () {
            $('input[name="radio1"]').prop('disabled', true);
        });

        $('input[name="radio1"]').on('change',function () {
            $('.btn-primary').removeAttr('hidden');
        });
    });
</script>


    <a href="/pages/Game%20questions/question2.php" class="btn btn-primary" id="btn-next" hidden>next question</a>

    <div id ="answers-trackers">

    </div>
</body>
</html>
