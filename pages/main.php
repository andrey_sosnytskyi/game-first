<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script defer src="https://pro.fontawesome.com/releases/v5.10.0/js/all.js" integrity="sha384-G/ZR3ntz68JZrH4pfPJyRbjW+c0+ojii5f+GYiYwldYU69A+Ejat6yIfLSxljXxD" crossorigin="anonymous"></script>

<link rel="stylesheet" href="/pages/styles%20for%20main%20page.css">
<div id ="jumbotron"></div>
<div class="jumbotron jumbotron-fluid" style="border-radius: 20px;">
    <div class="container">
        <h1 class="display-4">Play Quiz-game and sharp your brain!</h1>
        <p class="lead">Check your knowledge and proof that you're the best!</p>
    </div>


    <div class="card">
        <div class="card-body" style=" border: 6px solid #e0d3f6; border-radius: 15px 100px 15px 100px; background-color: #eafad6;" >

<!--
            <div id="carouselExampleIndicators" class="carousel slide col-lg-3" data-ride="carousel" >
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="/resources/coins.jpg" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="/resources/scwosh.jpg" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="/resources/toyota_logo1.jpg" alt="Third slide">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>-->

            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class ="card">
                            <div class="card-header bg-success ">
                                <h3 class="text-white text-center">Quiz &nbsp;<i class="fa fa-comment-dots"></i></h3>
                            </div>
                            <h2 class="text-center ">Offical info</h2>
                            <p class="text-center mr-auto">
                            <ul>
                                <li>Check your knowledge!</li>
                                <li>Show your talent to others!</li>
                                <li>Prove that you're the first and the best!</li>
                            </ul>
                             </p>

                            <a class="btn btn-outline-success" href="/pages/dropdown1.html">Learn more</a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class ="card">
                            <div class="card-header bg-info ">
                                <h3 class="text-white text-center">Rules &nbsp; <i class="fa fa-solid fa-book-open"></i></h3>
                            </div>
                            <h2 class="text-center ">Required:</h2>
                            <p class=" text-center mr-auto">
                                <ul>
                                <li>Not to use Browser to find correct answers</li>
                                <li>You must read instructions before begin to play</li>
                                </ul>
                            </p>
                            <a class="btn btn-outline-info" href="/pages/dropdown1.html">Learn more</a>

                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class ="card">
                            <div class="card-header bg-warning ">
                                <h3 class="text-white text-center">About project &nbsp; <i class="fas fa-tasks"></i></h3>
                            </div>
                            <h5 class="text-center ">Endless ideas</h5>
                            <p class=" text-center mr-auto">

                                The quiz was created to check people's knowledge and outlook! The game is going to be updated and improved periodically! It will change our imagination!<br/>Enjoy the game!
                            </p>
                            <a class="btn btn-outline-warning" href="/pages/dropdown1.html">Learn more</a>

                        </div>
                    </div>
                </div>
            </div>

            <div class="card-text badge badge-light col-md-5 " style="margin-top: 2%;" >
                <h5><i>"...The roots of education are bitter,<br/> but the fruit is sweet..."</i></h5>
                <p><i>Aristotle</i></p></div>
            <a href="#" class="btn btn-primary" style="margin-top: 20px;margin-right: 70%;margin-left: 1%" >Go somewhere</a>
<?php
if (isset($_SESSION['username']) && $_SESSION['uid']) {
    $user_obj = new User();
    $user = $user_obj->getUserById($_SESSION['uid']);
    $user_first_name = $user['first_name'];
    $user_last_name = $user['last_name'];
    echo '<div class="arrow-down"><span></span> <span></span> <span></span></div>';
}

?>
        </div>
        </div>
    </div>

<!-- col-xs-10 col-md-8 col-sm-10 col-lg-8 col-xl-8 -->

<?php
if (isset($_SESSION['username']) && $_SESSION['uid']) {
    $user_obj = new User();
    $user = $user_obj->getUserById($_SESSION['uid']);
   $user_first_name = $user['first_name'];
    $user_last_name = $user['last_name'];
   // echo '<a href= "/pages/Game%20questions/question1.php" class="btn btn-block btn-success btn-lg  ">Start!</a>';
    echo '<a href= "/pages/loader/loader.html" class="btn btn-block btn-success btn-lg ">Start! &nbsp; <i class="fas fa-flag-checkered"></i></a>';
}
// сделать анимацию для каждого блока главной страницы
?>
<h1>Hello Everyone!</h1>

<?php
//global $mysqli;
//
//$result= $mysqli->query("SELECT * FROM users");
//
//while ($res = $result->fetch_assoc()){
//    echo $res['id'];
//}
