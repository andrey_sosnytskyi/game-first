<?php

session_start();
define("ABSPATH",true);
require_once 'system/configuration.php';
require_once 'system/classes/User.php';
require_once 'blocks/header.php';

if(isset($_GET['page'])) {
    switch ($_GET['page']) {
        case "main":
            require_once "pages/main.php";
            break;
        case "users_list":
            require_once "pages/users_list.php";
            break;
        case "user_edit":
            require_once "pages/user_edit.php";
            break;
        case "user_view":
            require_once "pages/user_view.php";
            break;


        default:
            require_once "pages/404.php";
    }
}else{
    echo "<script>location.replace('/?page=main');</script>";
}
require_once 'blocks/footer.php';
?>