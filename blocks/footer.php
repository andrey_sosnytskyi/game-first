
<!--
<script src="/assets/js/jquery-3.6.0.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/bootstrap.bundle.min.js"></script>
-->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script defer src="https://pro.fontawesome.com/releases/v5.10.0/js/all.js" integrity="sha384-G/ZR3ntz68JZrH4pfPJyRbjW+c0+ojii5f+GYiYwldYU69A+Ejat6yIfLSxljXxD" crossorigin="anonymous"></script>


<footer class="bg-dark text-success mx-auto" style="padding:40px 0;">
    <div class="container">
        <div class="d-sm-flex justify-content-between">
            <div  class="footer-left" >
                <a style="margin-right: 20px" href="#">Terms and conditions</a>
                <a style="margin-right: 20px" href ="#">Privacy</a>
            </div>
            <div  class="footer-right">
                <span> Follow me </span>

                <a style="margin-left: 20px" id ="link" href="https://www.instagram.com/4ndrey_sosn/"><i class="fab fa-instagram"></i></a>
                <a style="margin-left: 20px" id ="link" href="https://www.facebook.com/profile.php?id=100072161782687"><i class="fab fa-facebook"></i></a>
                <a style="margin-left: 20px" id ="link" href="https://t.me/andrey_sosn"><i class="fab fa-telegram"></i></a>
            </div>
        </div>

    </div>
    <div style="width:200px;height:50px;vertical-align:bottom;display: flex;align-items: flex-end;float: right">
        <p class="text-light text-right text-light" style=" font-size: small; font-style: normal; text-decoration-line:underline; " >
            All rights reserved 2022 </p>
    </div>
</footer>
</body>
</html>
