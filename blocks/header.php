<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <!--<script src="/assets/js/jquery-3.6.0.min.js"></script>
    <script src="/assets/js/bootstrap.bundle.js"></script>
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/bootstrap-grid.min.css"> -->

   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script defer src="https://pro.fontawesome.com/releases/v5.10.0/js/all.js" integrity="sha384-G/ZR3ntz68JZrH4pfPJyRbjW+c0+ojii5f+GYiYwldYU69A+Ejat6yIfLSxljXxD" crossorigin="anonymous"></script>

    <!-- http://game.local/?page=main -->

    <title>Quiz game</title>
</head>

 <body style="background: url('/resources/game picture.jpg') no-repeat;width: 100%;height:100vh;background-size: cover;  ">

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#"><span>&#127942;</span> Quiz game <span>&#127881;</span></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="http://game.local/?page=main">Home &nbsp; <i class="fas fa-home"></i> <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/verifying/verifying.php">Users list</a>
                <!-- href="http://game.local/?page=users_list" -->
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
                   aria-expanded="false">
                    More
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="/pages/dropdown1.html"> Game </a>
                    <a class="dropdown-item" href="/pages/dropdown2.html"> About me </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">About project </a>
                </div>
            </li>

            <li class="nav-item">
                <a class="nav-link disabled">Disabled</a>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <?php
            if (isset($_SESSION['username']) && $_SESSION['uid']) {
                $user_obj = new User();
                $user = $user_obj->getUserById($_SESSION['uid']);
                $user_first_name = $user['first_name'];
                $user_last_name = $user['last_name'];

                ?>
                <span class="text-white"><?php echo $user_first_name . '&nbsp;' . $user_last_name; ?></span>&nbsp;&nbsp;&nbsp;
                <a class="btn btn-outline-danger my-2 my-sm-0" href="/logout.php">Log out -></a>
            <?php } else { ?>
                <a class="btn btn-outline-success my-2 my-sm-0" href="/login.php">Login</a> &nbsp;&nbsp;&nbsp;
                <a class="btn btn-outline-primary my-2 my-sm-0" href="/register.php">Register</a>
            <?php } ?>
        </form>
    </div>
</nav>



