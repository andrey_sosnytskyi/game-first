/* All answer options */

const input1 = document.querySelector('.input1'),
      input2 = document.querySelector('.input2'),
      input3 = document.querySelector('.input3'),
      input4 = document.querySelector('.input4');

const label1 = document.querySelector('#label1'),
      label2 = document.querySelector('#label2'),
      label3 = document.querySelector('#label3'),
      label4 = document.querySelector('#label4');

/* All our options */

const btnElements = document.querySelectorAll('.btn');

const question = document.querySelector('.form-check'),
      numberOfquestion = document.getElementById('number-of-question'),
      numberOfAllQuestions = document.getElementById('number-of-all-question');

let indexOfQuestion, //индекс текущего вопроса
    indexOfPage =  0; //индекс страницы

const answersTracker = document.getElementById('answers-tracker');
const btnNext = document.getElementById('btn-next');

let score = 0; //Final result of the quiz

const correctAnswer = document.getElementById('correct-answer'),
      numberOfAllQuestion2 = document.getElementById('number-of-all-questions-2'),
        percentage = document.getElementById('percentage'),
        percent = document.getElementById('percent'),
        attempts = document.getElementById('attempts');
        success = document.getElementById('success');

 const btnTruAgain = document.getElementById('btn-try-again');

 var counter = 2;

// Main header h2

let answer = document.querySelector('.h2');

// Array of names and scores

//let scoreBtn = document.getElementById('scoreBtn');
//let user_name = document.getElementById('user_name');

var time = 30;

    function timer() {
        const timeBar = document.getElementById('timeBar');
        if(time > 0) {
            time--;
        } else {
                updateAnswerTracker('wrong');
                disabledOptions();
                answer.innerHTML ='Вы ничего не выбрали!';
                //console.log('test');
            }
        timeBar.value = time / 30*100 ;

           /*  setTimeout(function () {
                if (start <= 0) {
                    //clearInterval(interval);
                   /* if(interval == 0) {
                        checkAnswer();
                        disabledOptions();

                    } */
                /*else {
                        updateAnswerTracker('wrong');
                        disabledOptions();
                        answer.innerHTML = 'Вы ничего не выбрали!';
                    }

                } else {
                        timeBar.value = start;
                    }
                timeBar.value = start;
                start--;

            }, 1000); */

        }
        var tm = setInterval(timer,1000);
    timer();

btnNext.addEventListener('click',function () {
    timerRefresh();
    timer();

});

function timerRefresh() {
    time = 30;
}

let resultArray = [];

const questions =[
    {
        question: 'Во языке программирования JavaScript  выражение "14" + 1 равно...',
        options: [
            '15',
            '141',
            '114',
            '24',

        ],
        rightAnswer: 1
    },
    {
        question: 'Какой танк был полностью разработан Харьковским заводом транспортного машиностроения имени Малышева и должен был стоять возле исторического музея г.Харькова?',
        img:'/resources/istorichnyi_mysei.jpg',
        options: [
            'т-64',
            'т-72',
            'т-62а',
            'т-90',

        ],
        rightAnswer: 0
    },
    {
        question:' Какой мировой компании принадлежит концерн "Bugatti" на сегодня',
        options: [
            'Infinity',
            'Mercedes',
            'Lamborghini',
            'Volkswagen',


             ],
        rightAnswer: 3

    },
    {
        question: 'Какой самый распространенный язык в мире ?',
        options: [
            'Английский',
            'Испанский',
            'Китайский',
            'Арабский',

        ],
        rightAnswer: 2
    },
    {
        question: 'Как правильно называетя боковая грань монет ?' ,
        img: '/resources/coins.jpg',
        options: [
            'Румб',
            'Гурт',
            'Тарт',
            'Норд',

        ],
        rightAnswer: 1
    },
    {
        question: 'Какое название носит этот водопад ? ',
        img: '/resources/vodopad_viktoriya02.jpg',
        options: [
            'Виктория',
            'Учан-Су',
            'Ниагарский',
            'Анхель',

        ],
        rightAnswer: 0
    },
    {
        question: 'Что такое Попокатепетль ?',
        options: [
            ' Горный массив ',
            ' Желоб ',
            ' Вулкан ',
            ' Научно-исследовательский центр в Антарктиде ',

        ],
        rightAnswer: 2
    },
    {
        question: 'Как переводиться бренд "Samsung" с корейского ?',
        img: '/resources/samsung logo.jpg',
        options: [
            '"Бестселлер"',
            '"Четыре звезды"',
            '"Рекордсмен"',
            '"Три звезды"',


        ],
        rightAnswer: 3
    },
    {
        question: 'К какому из материков ближе всего находиться остров Огненная земля ?',
        img: '/resources/Vognana zemlya.jpg',
        options: [
            'Северная Америка',
            'Южная Америка',
            'Евразия',
            'Антарктида',

        ],
        rightAnswer: 1
    },
   {
        question: ' Кто написал стихотворение "Ще не вмерла...", которое со временем стало Гимном Украины ? ',
        options: [
            'М.Зеров',
            'М.Грушевский',
            'П.Чубинский',
            'М.Вербицкий',

        ],
        rightAnswer: 2
    },
    {
        question: 'Сколько ГБ весит секунда нашего зрения? ',
        img: '/resources/eyes.jpg',
        options: [
            '21,45 ГБ',
            '10,75 ГБ',
            '60,05 ГБ',
            '5,91 ГБ',

        ],
        rightAnswer: 0
    },
    {
        question: 'Самая глубокая в мире впадина это... ',
        options: [
            'Кольская',
            'Марианская',
            'Пуэрто-Рико',
            'Зондская',

        ],
        rightAnswer: 1
    },
   /* {
        question: 'VVedite',
        options: [
            document.createElement('input').innerHTML ="<input type='text' />",
        ],
        rightAnswer: ','
    },*/
];
document.querySelector('#number-of-all-questions').innerHTML = questions.length;  //  колличество всех вопросов

const load =function () {
 document.querySelector('#question').innerHTML = questions[indexOfQuestion].question; // сам вопрос

 //console.log( question[indexOfQuestion]);
    document.querySelector('.question-image').innerHTML = '';
   if( questions[indexOfQuestion].img ) {

        document.querySelector('.question-image').innerHTML = "<img src='" + questions[indexOfQuestion].img + "'>"
    }

   /* if( indexOfQuestion === questions.length-1) {
        btnNext.innerHTML = "Finish!";
    } */

    input1.innerHTML = questions[indexOfQuestion].options[0];
    input2.innerHTML = questions[indexOfQuestion].options[1];
    input3.innerHTML = questions[indexOfQuestion].options[2];
    input4.innerHTML = questions[indexOfQuestion].options[3];


numberOfquestion.innerHTML = indexOfPage + 1; // установка номера текущей страницы

indexOfPage++; //увеличение индекса страницы

}

let completedAnswers = [];

const  randomQuestion =function () {
    let randomNumber = Math.floor(Math.random() * questions.length);
    let hitDublicate = false;

    if(indexOfPage === questions.length){
        quizOver();
    }
    else  {
        if (completedAnswers.length > 0){
            completedAnswers.forEach(item => {
                if(item === randomNumber){
                    hitDublicate = true;
                }
            });
            if (hitDublicate){
                randomQuestion();
            }
            else {
                indexOfQuestion = randomNumber;
                load();
            }
        }
        if (completedAnswers.length === 0){
            indexOfQuestion = randomNumber;
            load();
        }

    }

    completedAnswers.push(indexOfQuestion);
}

const checkAnswer = el => {
if(el.target.dataset.id == questions[indexOfQuestion].rightAnswer){
    el.target.classList.add('correct');
    updateAnswerTracker('correct');
    answer.innerHTML='правильно!';
    score++;

}
else {
    el.target.classList.add('wrong');
    updateAnswerTracker('wrong');
    answer.innerHTML ='неправильно!';

}

    disabledOptions();

}
const disabledOptions= () => {
  btnElements.forEach(item =>{
      item.classList.add('disabled');
      if(item.dataset.id == questions[indexOfQuestion].rightAnswer){
          item.classList.add('correct');
      }
  });
}
const enableOptions = () => {
  btnElements.forEach(item => {
      item.classList.remove('disabled','correct','wrong');

  });
};

const answerTracker = () => {
    answersTracker.innerHTML = "";   // nullify answerTracker
  questions.forEach(() => {
      const div =document.createElement('div');
      answersTracker.appendChild(div);
  })
}

const updateAnswerTracker = status => {
    console.log(answersTracker.children)
  answersTracker.children[indexOfPage - 1].classList.add(`${status}`);

}

const validate = () => {
  if(!btnElements[0].classList.contains('disabled')){
      alert('Please,choose the answer!');
  }
  else  {
      randomQuestion();
      enableOptions();

  }
};

btnNext.addEventListener('click',validate);

for( let btn of btnElements){
    btn.addEventListener('click',e =>checkAnswer(e));
}

const quizOver = function () {

    document.querySelector('.quiz-over-modal').classList.remove('invisible');
    //document.querySelector('.quiz-over-modal').classList.add('active');

    correctAnswer.innerHTML = score;
    numberOfAllQuestion2.innerHTML = questions.length;
    percentage.innerHTML = Math.round((score / questions.length) * 100);

    if (score <= 4) {
        percent.classList.add('badge-danger');
        success.innerHTML = "У вас очень мало правильных ответов!" + '&#128533;';
    }
    if ((score>=5) && (score <= 9)) {
        percent.classList.add('badge-warning');
        success.innerHTML = "Неплохо!" + '&#128527;';
    }
    if ((score >= 10) || (score == questions.length)) {
        percent.classList.add('badge-success');
        success.innerHTML = "Превосходно!" + '&#128571;';
    }


    attempts.innerHTML = counter - 1; // quantity of the attempts

    btnTruAgain.addEventListener('click', function () {
    tryAgain();
    if (counter == 0){
        location.replace('http://game.local/?page=main');
    }
    });

};

const tryAgain = () => {
    document.querySelector('.quiz-over-modal').classList.add('invisible');
    completedAnswers = []; // nullify answers
    indexOfPage = 0;
    randomQuestion();
    enableOptions();
    answerTracker();
    score = 0;
 // window.location.reload();
    counter--;
};

/*
class Timer {
    constructor(timeBarDOMElement) {
        this.timeBar = timeBarDOMElement;
        this.QuizTime = 20;
        this.timerTimeout = null;
    }
    start() {
        this.timeBar.classList.remove('invisible');
        this.timeBar.classList.add('timer-animation');

        const frame = () => {
          const timerEvent = document.createEvent('Event');
          timerEvent.initEvent('timeout',false,false);
          document.dispatchEvent(timerEvent);
        }
        this.timerTimeout = setTimeout(frame,this.QuizTime * 1000);
    }
    stop() {
        let width = this.timeBar.getBoundingClientRect().width;
        this.timeBar.classList.remove('timer-animation');
        this.timeBar.style.width = width +'px';
        this.timeBar.classList.add('invisible');
        clearTimeout(this.timerTimeout);
    }
}
*/
window.addEventListener('load',function () {
    randomQuestion();
    answerTracker();


});

// name and score validation

/*const  form = document.getElementById('form');

function retrieveFormValue(event) {
    event.preventDefault();

    const name = form.querySelector('[name="name"]');

    const  values = {
        name: name.value,score,
    }
    console.log(values);

}
localStorage.setItem(score,values);
form.addEventListener('submit',retrieveFormValue); */








