<!DOCTYPE html>
<html lang="en">
<head>
    <title></title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="/verifying/verifying.css">
</head>
<body>

<!-- Modal -->

<div class =" modal fade show" style="display: block;" tabindex="-1" role="dialog" aria-hidden="true"  >
    <div class="modal-dialog content" role="document">
        <div class="modal-content bg-light border-warning">
            <div class="modal-header">
                Enter password to view and edit information about users
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                      <label for="password">Enter password:
                        <input type="password" class="form-control " id="password" value=""/>
                     </label>
                    </div>

                    <div class="form-group form-check">
                         <label>
                         <input class="form-check-input checkbox" type="checkbox"/> Show password
                    </label>
                    </div>

                    <div>
                        <input class="btn btn-danger" type="reset" value="Reset"/>
                    </div>
                </form>

            </div>
            <div class="modal-footer">
                <a class="btn btn-success" href="http://game.local/index.php?page=main">Main page</a>
            </div>

        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('form').on('click','.checkbox',function () {
            if($(this).is(':checked')){
                $('#password').attr('type','text');
            } else  {
                $('#password').attr('type','password');
            }
        });
    });
</script>
<script>
    document.addEventListener('input',function () {

    var password = document.getElementById('password') ;
    if(password.value === '8974'){
        location.replace('http://game.local/?page=users_list');
    }
        });
</script>
</body>
</html>
